$:.push File.expand_path("../lib", __FILE__)

require "conjure_iap_verification/version"

Gem::Specification.new do |s|
  s.name        = "conjure_iap_verification"
  s.version     = ConjureIapVerification::VERSION
  s.authors     = ["Conjure"]
  s.email       = ["technical@conjure.co.uk"]
  s.homepage    = "http://www.conjure.co.uk"
  s.summary     = "Encapsulates ending requests to a Conjure IAP Verification Server."
  s.description = "Provides an easy way of sending requests to a Conjure Verification Server to ensure in-app purchases from mobile apps are genuine."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "httparty", "~> 0.11.0"
  s.add_dependency "json", "~> 1.8.0"

  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'webmock'
end

# Generate the rdoc for the gem with rdoc --main README.rdoc --exclude "/doc"
