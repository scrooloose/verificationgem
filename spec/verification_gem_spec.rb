require 'spec_helper'

describe ConjureIapVerification do
  it "should return a successful response with a valid receipt (iOS)" do
    stub_request(:post, "http://server/verify").
            to_return(:status => 200, :body => '{ "status" : "0" }')
    verification = ConjureIapVerification.verify(server: "http://server", auth_token: "MYAUTH", bundle_id: "uk.co.conjure.awesomeapp", transaction_details: "transaction-receipt")
    verification.should_not be_nil
    
    verification.should be :valid_purchase
  end
  
  it "should return a successful response with a valid receipt (Google)"
  
  it "should ensure restored transactions are marked as such"
  
  it "should cope with missing data via sensible return codes"
  
  it "should ensure all required fields are present, and throw appropriate exceptions if not"
  
  it "should throw an unauthorised exception in the case of failing authorisation"
end