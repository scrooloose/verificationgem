require 'conjure_iap_verification/version'
require 'conjure_iap_verification/conjure_iap_verification_api'

module ConjureIapVerification
  # Constant to be used when sending a verification for an iOS store purchase
  IOS_STORE = "ios"
  
  # Constant to be used when sending a verification for an Android (Google Play) store purchase
  GOOGLE_STORE = "google"
  
  # A collection of supported android stores
  ANDROID_STORES = [GOOGLE_STORE]
  
  # Used to indicate the receipt should be verified as a purchase against a live server
  LIVE = "live"
  
  # Used to indicate the receipt should be verified as a purchase against a sandbox server
  SANDBOX = "sandbox"
  
  # Call this to verify a transaction from an in-app purchases.
  #
  # Any required options that are missing will raise a ConjureIapVerificationApiArgumentError. Calling code should deal with this.
  #
  # The response is one of +:valid_purchase+, +:valid_restore+, +:invalid+, +:incomplete_or_invalid_information+, or +:sever_error+
  #
  # ==== Attributes
  #
  # * +options+ - The various options that are needed to verify items. Some are mandatory.
  #
  # ==== Options
  #
  # * +:server+               - The URL of verification server to use. Mandatory.
  # * +:auth_token+           - A token which will be used to allow access to the verification server. Mandatory.
  # * +:bundle_id+            - The bundle/package name of the app, which must match an app the verification server knows about. Mandatory.
  # * +:store_type+           - A string indicating which store (+IOS_STORE+ or +GOOGLE_STORE+) this purchase is from. If not provided defaults to +IOS_STORE+.
  # * +:transaction_details+  - For ios, a base64 encoded string of the transaction details. For android, the signed data returned from a transaction as a plaintext JSON string. Order of items in that hash is vital, so send as the returned string from the store. Mandatory.
  # * +:signature+            - For android only. Mandatory if +:store_type+ is +GOOGLE_STORE+. Should be the signature sent back to +purchaseStateChanged+ and that which has been used with the +:transaction_details+.
  # * +:live_or_sandbox+      - A string indicating whether it should verify against the live or sandbox verification servers. Defaults to +SANDBOX+ if not provided.
  #
  # ==== Examples
  #
  # Some examples:
  #
  #   verify = ConjureIapVerificationApi.verify(server: "http://myserver", auth_token: "MYAUTHTOKEN", bundle_id: "uk.co.conjure.app", transaction_details: "ewoJInNpZ25hdHVyZSIgPSAiQW51ZzNzTFl6NVE0S2VTTmFRSGxQT...")
  #
  def self.verify(options)
    ConjureIapVerificationApi.verify(options)
  end
end
