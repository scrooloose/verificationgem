require 'httparty'
require 'json'

# A gem to allow easy access to Conjure's verification server for in-app purchases. Each verification
# request takes the server to use so it makes testing, and load balancing between servers, very easy.
module ConjureIapVerification
  # Base class for any Verification Gem errors/exceptions
  class ConjureIapVerificationError < StandardError
  end
  
  # Raised when required options are not provided for a verification.
  class ConjureIapVerificationApiArgumentError < ConjureIapVerificationError
    # The option that has an error on it. See the message for more details.
    attr_reader :argument
    
    def initialize(msg = "Required option is missing", arg = "")  #:nodoc:
      super msg
      @argument = arg.to_sym
    end
  end
  
  # Raised when verification fails with the remote server
  class ConjureIapVerificationApiAuthorisationError < ConjureIapVerificationError
  end
  
  # The main class used to communicate with the API
  class ConjureIapVerificationApi
    include HTTParty

    # Class method to actually perform the verification. For details see ConjureIapVerification#verify which should be used instead as wrapper around this code. This method signature is not guaranteed whereas the main one will be made conistent for gem users.
    def self.verify(options)
      required = Hash.new do |h, k|
        raise ConjureIapVerificationApiArgumentError.new("#{k} is required in the options", k)
      end
      required.update(options)

      server = required[:server]
      auth_token = required[:auth_token]
      bundle_id = required[:server]
      store_type = options[:store_type] || ::ConjureIapVerification::IOS_STORE
      signature = required[:signature] if store_type == ::ConjureIapVerification::GOOGLE_STORE
      transaction_details = required[:transaction_details]
      live_or_sandbox = options[:live_or_sandbox] || ::ConjureIapVerification::SANDBOX
      
      if store_type == ::ConjureIapVerification::GOOGLE_STORE
        transaction_details = "#{transaction_details}\"signature\":\"#{signature}\""
        store_type = "android"  # The server doesn't differentiate android stores at this point
      end
      
      params = {
        auth_token: auth_token,
        bundle_id: bundle_id,
        store_type: store_type,
        transaction_details: transaction_details,
        live_or_sandbox: live_or_sandbox
      }

      self.base_uri server
      response = self.post('/verify', body: params)
      json = JSON.parse(response.body)
      
      if json["status"] == "-1"
        raise ConjureIapVerificationApiAuthorisationError.new(json["errors"]["401"].first)
      elsif json["status"] == "0"
        return (json["restore"] ? :valid_restore : :valid_purchase)
      elsif ["500", "600"].include? json["status"]
        return :invalid
      elsif ["100", "404", "620", "630", "640", "660", "670", "680"].include? json["status"]
        return :incomplete_or_invalid_information
      else  # "400", "650", "690", "700"
        return :server_error
      end
    end
  end
end