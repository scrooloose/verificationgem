# Conjure In-App Purchase Verification Gem

The conjure-iap-verification gem provides a simple and convenient way to validate In-App Purchases (IAPs) purchased (or restored) through the Apple App Store or the Google Play Store.

This gem works hand in hand with our cloud-hosted verification server, and is useless without an API key.  If you need a scaleable verification service, then please [get in touch](mailto:technical@conjure.co.uk) for pricing and an API key.

For the system to work most effectively, we also need to configure bundle/package names and your public key for each purchase you are looking to verify.

## Features

* Independent confirmation of legitimate In-app Purchases (IAPs) through verification of store receipts from Apple App Store and Google Play Store
* In-built IAP Product database with the ability to send product specific data on a verified response, for example, a URL for downloadable IAP content
* Supports generating an AES decryption key and IV pair,  created using unique device information to prevent decryption keys being shared between devices
* Supports verification of store receipts from new IAPs or restored purchases and can identify the difference between the two (useful for determining hacked purchases)
* Logging of all verification attempts for statistics monitoring and fraud analysis including determining repeat users.
* Hosted on Heroku (AWS) providing instant scalability if high throughput is required
* Supports sandbox mode for testing pre-release IAPs
* Access to the a simple and secure (HTTPS) JSON-based RESTful interface if required



## Installation for Rails > 3.1
Include conjure-iap-verification in your Gemfile;

```ruby
gem 'conjure-iap-verification', :git => 'https://bitbucket.org/conjure/verificationgem.git'
```

and run bundle install.

## Usage

Include the library where you want to use it

```ruby
require 'conjure_iap_verification'
```

Call ```ConjureIapVerification.verify(options)``` with an options hash containing:

### Options
 
* __:server__               - The URL of verification server to use. Mandatory.
* __:auth_token__           - A token which will be used to allow access to the verification server. Mandatory.
* __:bundle_id__            - The bundle/package name of the app, which must match an app the verification server knows about. Mandatory.
* __:store\_type__           - A string indicating which store (*IOS_STORE* or *GOOGLE_STORE*) this purchase is from. Defaults to *IOS_STORE*
* __:transaction_details__  - For iOS, a base64 encoded string of the transaction details. For android, the signed data returned from a transaction as a plaintext JSON string. Order of items in that hash is vital, so send as the returned string from the store. Mandatory.
* __:signature__            - (Google Play only). Mandatory if __:store\_type__ is *GOOGLE_STORE*. Should be the signature sent back to *purchaseStateChanged* and that which has been used with the __:transaction\_details__.
* __:live\_or\_sandbox__      - A string indicating whether it should verify against the live or sandbox verification servers. Defaults to *SANDBOX*.
 
### Examples

A Google Play example:

```ruby
ConjureIapVerification.verify({
  server: "https://conjure-verification-server-url", 
  auth_token: "your-auth-token", 
  bundle_id: "com.package.name", 
  transaction_details: '{"nonce":99999999999,"orders":[{"notificationId":"android.test.purchased","orderId":"transactionId.android.test.purchased","packageName":"com.package.name","productId":"android.test.purchased","purchaseTime":1343733900920,"purchaseState":0}]}',
  signature: "signaturestring===",
  store_type: ConjureIapVerification::GOOGLE_STORE
})
```

An Apple App Store example:

```ruby
ConjureIapVerification.verify({
  server: "https://conjure-verification-server-url",
  auth_token: "your-auth-token", 
  bundle_id: "com.package.name", 
  transaction_details: 'your-base-64-encoded-transaction-details'
})
```

## Questions? Bugs?

Please [get in contact](mailto:technical@conjure.co.uk).

## License

Copyright 2013 Conjure Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
